variable "vpc_name" {
  default = "sph_media"
}

variable "vpc_cidr" {
  default = "10.10.2.0/24"
}

variable "account_owner" {
  default = "uyanushka"
}

variable "aws_account" {
  default = "AWS-SHP-DEMO-DEV"
}

variable "app_name" {
  default = "urlcheck"
}

variable "common_tags" {
  default = {
    "data_classifiaction" = "N/A",
  }
}

variable "region" {
  default = "us-east-1"
}

variable "vpc_endpoint_egress" {
  default = [
    {
      "description" : "test-sg-rule-ipbased",
      "from_port" : 0,
      "to_port" : 0,
      "protocol" : "-1",
      "cidr_blocks" : "0.0.0.0/0"
    }
  ]
}