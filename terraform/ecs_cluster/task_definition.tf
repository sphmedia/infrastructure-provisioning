resource "aws_ecs_task_definition" "task_definition" {
  family                   = "report"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  container_definitions    = <<TASK_DEFINITION
[
  {
    "name": "demo-dev-report",
    "image": "<--AWS-ACCOUNT-ID-->.dkr.ecr.us-east-1.amazonaws.com/demo-dev-urlreport-ecr:latest",
    "cpu": 1024,
    "memory": 2048,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ]
  }
]
TASK_DEFINITION

}

resource "aws_ecs_task_definition" "schedule_task_definition" {
  family                   = "schedule_task"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  container_definitions    = <<TASK_DEFINITION
[
  {
    "name": "demo-dev-urlcheck",
    "image": "<--AWS-ACCOUNT-ID-->.dkr.ecr.us-east-1.amazonaws.com/demo-dev-urlcheck-ecr:latest",
    "cpu": 1024,
    "memory": 2048,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ]
  }
]
TASK_DEFINITION

}
