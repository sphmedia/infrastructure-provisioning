resource "aws_ecs_service" "main" {
  name                               = "report"
  cluster                            = aws_ecs_cluster.fargate.id
  task_definition                    = aws_ecs_task_definition.task_definition.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = [data.aws_security_group.selected.id]
    subnets          = [data.aws_subnet.private-subnet-1.id, data.aws_subnet.private-subnet-2.id]
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = data.aws_lb_target_group.target_group.arn
    container_name   = "demo-dev-report"
    container_port   = "8080"
  }

}
