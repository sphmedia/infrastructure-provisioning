data "aws_subnet" "private-subnet-1" {
  filter {
    name   = "tag:Name"
    values = ["private-subnet-1"]
  }
}

data "aws_subnet" "private-subnet-2" {
  filter {
    name   = "tag:Name"
    values = ["private-subnet-2"]
  }
}

data "aws_security_group" "selected" {
  filter {
    name   = "tag:Name"
    values = ["DEMO-DEV-urlcheck-fargate-sg"]
  }
}

data "aws_lb_target_group" "target_group" {
  #   arn  = var.lb_tg_arn
  name = "DEMO-DEV-urlcheck-alb-tg"
}