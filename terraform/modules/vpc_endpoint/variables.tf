variable "vpc_id" {
  description = "id of the vpc"
}

variable "service_name" {
  description = "service name com.amazonaws.<region>.<service>"
}

variable "security_group_ids" {
  description = "list id of the security groups"
}

variable "subnet_ids" {
  description = "The ID of one or more subnets in which to create a network interface for the endpoint"
}