resource "aws_vpc_endpoint" "endpoint" {
  vpc_id       = var.vpc_id
  service_name = var.service_name
  vpc_endpoint_type = "Interface"
  private_dns_enabled = true
  security_group_ids = [var.security_group_ids]
  subnet_ids = var.subnet_ids
}