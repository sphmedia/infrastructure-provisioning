variable "name" {
  description = "name of the security group"
}

variable "description" {
  description = "Security group description. Defaults to Managed by Terraform. Cannot be ''"
}

variable "vpc_id" {
  description = "ID of the vpc where the security group going to associate"
}

variable "ipsource_ingress" {
  description = "IP based rule map"
}

variable "sgsource_ingress" {
  description = "security group based rule map"
}

variable "egress" {
  description = "egress rule map"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}