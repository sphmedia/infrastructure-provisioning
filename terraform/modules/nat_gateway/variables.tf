variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}

variable "igw_required" {
  description = "Do you need to attach igw on vpc, supported values true / false"
  default     = false
}

variable "subnet_id" {
  description = "id of the subnet"
}