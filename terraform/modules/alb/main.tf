resource "aws_lb" "lb" {
  name               = var.name
  internal           = var.internal
  load_balancer_type = var.load_balancer_type
  security_groups    = var.security_groups
  subnets            = var.subnets
  idle_timeout = var.idle_timeout
  
  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "alb",
        "Region"          = data.aws_region.current.name
      }
    )
  )

}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb.id
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = var.target_group_arn
    type             = var.type
  }
}