variable "name" {
  description = "name"
}

variable "internal" {
  description = "If true, the LB will be internal."
  default = false
}

variable "load_balancer_type" {
  description = "The type of load balancer to create"
  default = "application"
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the LB"
}

variable "subnets" {
  description = "A list of subnet IDs to attach to the LB."
}

variable "target_group_arn" {
  description = "ARN of the Target Group to which to route traffic"
}

variable "type" {
  description = "Type of routing action"
  default = "forward"
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle"
  default = 300
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}