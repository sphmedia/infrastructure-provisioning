resource "aws_route_table" "route_table" {
  vpc_id = var.vpc_id

  dynamic "route" {
    for_each = var.routes

    content {
      cidr_block                = route.value["destination_cidr"]
      carrier_gateway_id        = lookup(route.value,"carrier_gateway_id", null)
      egress_only_gateway_id    = lookup(route.value,"egress_only_gateway_id", null)
      gateway_id                = lookup(route.value,"gateway_id", null)
      instance_id               = lookup(route.value,"instance_id", null)
      local_gateway_id          = lookup(route.value,"local_gateway_id", null)
      nat_gateway_id            = lookup(route.value,"nat_gateway_id", null)
      network_interface_id      = lookup(route.value,"network_interface_id", null)
      transit_gateway_id        = lookup(route.value,"transit_gateway_id", null)
      vpc_endpoint_id           = lookup(route.value,"vpc_endpoint_id", null)
      vpc_peering_connection_id = lookup(route.value,"vpc_peering_connection_id", null)

    }
  }

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "rtb",
        "Region"          = data.aws_region.current.name
      }
    )
  )
}