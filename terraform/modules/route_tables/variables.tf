variable "vpc_id" {
    description = "ID of the vpc"
}

variable "routes" {
    description = "routing rules, supported all type of gateways"
}

variable "name" {
    description = "name of the route table"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}