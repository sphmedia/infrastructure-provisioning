resource "aws_s3_bucket" "s3bucket" {
  bucket = var.bucket_name

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "sg",
        "Region"          = data.aws_region.current.name
      }
    )
  )
}