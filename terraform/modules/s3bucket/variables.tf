variable "bucket_name" {
  description = "name of the aws bucket"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}