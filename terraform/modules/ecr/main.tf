resource "aws_ecr_repository" "ecr" {
  name                 = var.name
  image_tag_mutability = var.image_tag_mutability

  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "ecr",
        "Region"          = data.aws_region.current.name
      }
    )
  )
}