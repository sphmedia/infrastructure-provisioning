variable "name" {
  description = "name of the ecr registry"
}

variable "image_tag_mutability" {
  description = "The tag mutability setting for the repository"
  default =  "MUTABLE"
}

variable "scan_on_push" {
  description = "Indicates whether images are scanned after being pushed to the repository"
  default = false
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}