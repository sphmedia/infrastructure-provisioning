variable "name" {
  description = "name of the dynamodb table"
}

variable "hash_key" {
  description = "The attribute to use as the hash (partition) key"
}

variable "range_key" {
  description = "The attribute to use as the range (sort) key. Must also be defined"
}

variable "billing_mode" {
  description = "Controls how you are charged for read and write throughput and how you manage capacity."
  default = "PAY_PER_REQUEST"
}

variable "stream_enabled" {
  description = "Indicates whether Streams are to be enabled"
  default = true
}

variable "stream_view_type" {
  description = "When an item in the table is modified, StreamViewType determines what information is written to the table's stream"
  default = "NEW_AND_OLD_IMAGES"
}

variable "attributes" {
  description = "List of nested attribute definitions"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}