resource "aws_dynamodb_table" "example" {
  name             = var.name
  hash_key         = var.hash_key
  range_key = var.range_key
  billing_mode     = var.billing_mode
  stream_enabled   = var.stream_enabled
  stream_view_type = var.stream_view_type

  dynamic "attribute" {
    for_each = var.attributes

    content {
      name = attribute.value["name"]
      type   = attribute.value["type"]
    }
  }

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "dynamodb",
        "Region"          = data.aws_region.current.name
      }
    )
  )
}