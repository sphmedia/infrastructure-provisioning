locals {

  vnet_size = element(split("/", var.cidr_block), 1)

  subnet_sizes = [for item in var.subnet_config : (element(split("/", item.subnet_cidr), 1) - local.vnet_size)]

  calculated_cidrs = cidrsubnets(var.cidr_block, local.subnet_sizes...)

}