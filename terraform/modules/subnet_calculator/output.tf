output "subnet_config" {

  value = { for i in range(0, length(local.calculated_cidrs)) :
    var.subnet_config[i].name => {

      name        = var.subnet_config[i].name
      subnet_cidr = local.calculated_cidrs[i]

    }

  }

}