variable "vpc_id" {
  description = "The VPC ID to create in"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}

variable "igw_required" {
  description = "Do you need to attach igw on vpc, supported values true / false"
  default     = false
}