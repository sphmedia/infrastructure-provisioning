resource "aws_internet_gateway" "internet_gateway" {

  # count = var.igw_required == "true" ? 1 : 0

  vpc_id = var.vpc_id

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "igw",
        "Region"          = data.aws_region.current.name
      }
    )
  )
}