variable "name" {
  description = "name of the tgw"
}

variable "port" {
  description = "target port"
}

variable "protocol" {
  description = "target protocol"
}

variable "target_type" {
  description = "target type"
}

variable "vpc_id" {
  description = "vpc id where target are placed"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}