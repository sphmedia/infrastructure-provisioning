resource "aws_lb_target_group" "tgw" {
  name        = var.name
  port        = var.port
  protocol    = var.protocol
  target_type = var.target_type
  vpc_id      = var.vpc_id

  health_check {
    path                = "/health"
    port                = var.port
    protocol            = var.protocol
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200"
  }

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "target-group",
        "Region"          = data.aws_region.current.name
      }
    )
  )
}