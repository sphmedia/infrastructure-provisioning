variable "vpc_id" {
  description = "CIDR block of the VPC"
}

variable "availability_zone" {
  description = "name of the az"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}

variable "subnet_cidr" {
  description = "CIDR of subnet"
}
