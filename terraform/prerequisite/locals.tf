locals {

  vpc_prefix = substr(var.aws_account, 4, -1)

  resource_prefix = format("%s-%s", element(split("-", local.vpc_prefix), 1), element(split("-", local.vpc_prefix), 2))

  vpc_name = format("%s-%s-vpc", local.vpc_prefix, var.app_name)

  alb_sg_id = module.security_group[format("%s-%s-%s-sg", local.resource_prefix, var.app_name, "alb")].security_group_id

  subnet_configs = [
    {
      "name" : "dmz-subnet-1",
      "subnet_cidr" : "/26",
      "availability_zone" : "0"
    },
    {
      "name" : "dmz-subnet-2",
      "subnet_cidr" : "/26",
      "availability_zone" : "1"
    },
    {
      "name" : "private-subnet-1",
      "subnet_cidr" : "/26",
      "availability_zone" : "0"
    },
    {
      "name" : "private-subnet-2",
      "subnet_cidr" : "/26",
      "availability_zone" : "1"
    }
  ]

  fargate_security_rules = {
    fargate_ingress = [
      {
        "description" : "vpc endpoint ingress allow all",
        "from_port" : "8080",
        "to_port" : "8080",
        "protocol" : "TCP",
        "cidr_blocks" : var.vpc_cidr
      }
    ],
    fargate_egress = [
      {
        "description" : "test-sg-rule-ipbased",
        "from_port" : 0,
        "to_port" : 0,
        "protocol" : "-1",
        "cidr_blocks" : "0.0.0.0/0"
      }
    ]
  }

  alb_endpoint_security_rules = {
    vpc_endpoint_ingress = [
      {
        "description" : "alb allow port 80",
        "from_port" : "80",
        "to_port" : "80",
        "protocol" : "TCP",
        "cidr_blocks" : "0.0.0.0/0"
      }
    ],
    vpc_endpoint_egress = [
      {
        "description" : "allow all outbound",
        "from_port" : 0,
        "to_port" : 0,
        "protocol" : "-1",
        "cidr_blocks" : "0.0.0.0/0"
      }
    ]
  }

  security_group_config_map = [
    {
      name : format("%s-%s-%s-sg", local.resource_prefix, var.app_name, "fargate"),
      description : "vpc end point security group",
      ipsource_ingress : local.fargate_security_rules.fargate_ingress,
      sgsource_ingress : [],
      egress : local.fargate_security_rules.fargate_egress
    },
    {
      name : format("%s-%s-%s-sg", local.resource_prefix, var.app_name, "alb"),
      description : "vpc end point security group",
      ipsource_ingress : local.alb_endpoint_security_rules.vpc_endpoint_ingress,
      sgsource_ingress : [],
      egress : local.alb_endpoint_security_rules.vpc_endpoint_egress
    }
  ]

  route_table_config_map = [
    {
      name : format("%s-%s-%s-rtb", local.resource_prefix, var.app_name, "public")
      routes : [
        {
          destination_cidr : "0.0.0.0/0",
          gateway_id : module.internet_gateway.igw_id
        }
      ]
    },
    {
      name : format("%s-%s-%s-rtb", local.resource_prefix, var.app_name, "private")
      routes : [
        {
          destination_cidr : "0.0.0.0/0",
          gateway_id : module.nat_dateway.ngw_id
        }
      ]
    }
  ]

  route_table_association_cnfig_map = [
    {
      key : "route_1",
      route_table_id : module.route_table[format("%s-%s-%s-rtb", local.resource_prefix, var.app_name, "public")].route_table_id,
      subnet_ids : module.subnet["dmz-subnet-1"].subnet_id
    },
    {
      key : "route_2",
      route_table_id : module.route_table[format("%s-%s-%s-rtb", local.resource_prefix, var.app_name, "public")].route_table_id,
      subnet_ids : module.subnet["dmz-subnet-2"].subnet_id
    },
    {
      key : "route_3",
      route_table_id : module.route_table[format("%s-%s-%s-rtb", local.resource_prefix, var.app_name, "private")].route_table_id,
      subnet_ids : module.subnet["private-subnet-1"].subnet_id
    },
    {
      key : "route_4",
      route_table_id : module.route_table[format("%s-%s-%s-rtb", local.resource_prefix, var.app_name, "private")].route_table_id,
      subnet_ids : module.subnet["private-subnet-2"].subnet_id
    }
  ]

  nat_gateway_config_map = {
    name      = format("%s-%s-%s-eip", local.resource_prefix, var.app_name, "ngw")
    subnet_id = module.subnet["dmz-subnet-1"].subnet_id
  }

  ecr_config_map = [
    {
      name = format("%s-%s-ecr", local.resource_prefix, var.app_name)
    },
    {
      name = format("%s-%s-ecr", local.resource_prefix, "urlreport")
    }

  ]

  target_group_config_map = {
    name        = format("%s-%s-%s-tg", local.resource_prefix, var.app_name, "alb")
    port        = 8080
    protocol    = "HTTP"
    target_type = "ip"
  }

  alb_config_map = {
    name             = format("%s-%s-%s", local.resource_prefix, var.app_name, "alb")
    security_groups  = [module.security_group[format("%s-%s-%s-sg", local.resource_prefix, var.app_name, "alb")].security_group_id]
    subnets          = [module.subnet["dmz-subnet-1"].subnet_id, module.subnet["dmz-subnet-2"].subnet_id]
    target_group_arn = module.target_group.tg_arn
  }

  dynamodb_config_map = {
    name             = format("%s-%s-%s", local.resource_prefix, var.app_name, "db")
    hash_key = "time"
    range_key = "name"
    attributes = [
      {
        name = "time"
        type = "N"
      },
      {
        name = "name"
        type = "S"
      }
    ]
  }

  service_name = format("com.amazonaws.%s.ecr.dkr", var.region)
}