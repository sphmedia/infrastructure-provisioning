module "subnet_cidr" {

  source        = "../modules/subnet_calculator"
  cidr_block    = var.vpc_cidr
  subnet_config = local.subnet_configs

}

module "vpc" {

  #   for_each    = { for vpc in local.vpc_config_map : vpc.vpc_name => vpc }
  source      = "../modules/vpc"
  cidr_block  = var.vpc_cidr
  common_tags = merge(var.common_tags, { Name = upper(var.vpc_name) })
}

module "subnet" {
  source = "../modules/subnet"

  for_each = { for subnet_config in local.subnet_configs : subnet_config.name => subnet_config }

  vpc_id            = module.vpc.vpc_id
  availability_zone = data.aws_availability_zones.available.names[each.value.availability_zone]
  subnet_cidr       = lookup(module.subnet_cidr.subnet_config, each.value.name).subnet_cidr
  common_tags       = merge(var.common_tags, { Name = each.value.name })

}

module "internet_gateway" {

  source = "../modules/internet_gateway/"

  vpc_id      = module.vpc.vpc_id
  common_tags = merge(var.common_tags, { Name = upper(local.vpc_name) })

}

module "nat_dateway" {
  source      = "../modules/nat_gateway"
  subnet_id   = local.nat_gateway_config_map.subnet_id
  common_tags = merge(var.common_tags, { Name = local.nat_gateway_config_map.name })
}

module "route_table" {

  source = "../modules/route_tables/"

  for_each = { for route_table_config in local.route_table_config_map : route_table_config.name => route_table_config }

  name   = each.value.name
  routes = each.value.routes
  vpc_id = module.vpc.vpc_id
  common_tags = merge(var.common_tags, { Name = each.value.name })
}

module "dynamodb" {
  source = "../modules/dynamodb"
  name = local.dynamodb_config_map.name
  hash_key = local.dynamodb_config_map.hash_key
  range_key = local.dynamodb_config_map.range_key
  attributes = local.dynamodb_config_map.attributes
  common_tags = merge(var.common_tags, { Name = local.dynamodb_config_map.name })
}

module "routetable_association" {
  source = "../modules/route_table_association"

  for_each       = { for route_table_association_cnfig in local.route_table_association_cnfig_map : route_table_association_cnfig.key => route_table_association_cnfig }
  route_table_id = each.value.route_table_id
  subnet_id      = each.value.subnet_ids
}

module "ecr" {

  source   = "../modules/ecr"
  for_each = { for ecr_config in local.ecr_config_map : ecr_config.name => ecr_config }
  name     = lower(each.value.name)
  common_tags = merge(var.common_tags, { Name = each.value.name })

}

module "target_group" {
  source      = "../modules/alb_target_group"
  name        = local.target_group_config_map.name
  port        = local.target_group_config_map.port
  protocol    = local.target_group_config_map.protocol
  target_type = local.target_group_config_map.target_type
  vpc_id      = module.vpc.vpc_id
  common_tags = merge(var.common_tags, { Name = local.target_group_config_map.name })
}

module "alb" {
  source           = "../modules/alb"
  name             = local.alb_config_map.name
  security_groups  = local.alb_config_map.security_groups
  subnets          = local.alb_config_map.subnets
  target_group_arn = local.alb_config_map.target_group_arn
  common_tags = merge(var.common_tags, { Name = local.alb_config_map.name })
}

module "security_group" {
  source = "../modules/security_group"

  for_each = { for security_group_config in local.security_group_config_map : security_group_config.name => security_group_config }

  name             = each.value.name
  description      = each.value.description
  ipsource_ingress = each.value.ipsource_ingress
  sgsource_ingress = each.value.sgsource_ingress
  egress           = each.value.egress
  vpc_id           = module.vpc.vpc_id
  common_tags = merge(var.common_tags, { Name = each.value.name })

}

module "s3bucket" {
  source = "../modules/s3bucket"
  bucket_name = var.s3bucket_name
  common_tags = merge(var.common_tags, { Name = var.s3bucket_name })
}